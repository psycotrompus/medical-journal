package com.crossover.project;

import org.jasypt.util.password.PasswordEncryptor;
import org.jasypt.util.password.StrongPasswordEncryptor;
import org.junit.Test;

import static org.apache.logging.log4j.util.Strings.isBlank;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class PasswordEncoderTest {

	private PasswordEncryptor passwordEncryptor = new StrongPasswordEncryptor();

	@Test
	public void testPasswordEncoder() {
		final String password = "password";
		String encoded = passwordEncryptor.encryptPassword(password);
		assertNotNull(encoded);
		assertTrue(!isBlank(encoded));
		System.out.println(encoded);
	}

}
