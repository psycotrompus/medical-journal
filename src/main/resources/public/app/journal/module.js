(function(angular) {
    'use strict';

    var app = angular.module('journal');

    app.config(RouteConfig);

    RouteConfig.$inject = [ '$stateProvider', '$urlRouterProvider' ];

    function RouteConfig($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/home');

        $stateProvider.state('journal', {
            url: '',
            abstract: true,
            defaultChild: 'journal.home',
            views: {
                'header': {
                    templateUrl: 'app/common/views/menu.html',
                    controller: 'MenuController',
                    controllerAs: 'menuCtrl'
                },
                'contents': {
                    template: '<div ui-view="journal.contents"></div>'
                }
            }
        }).state('journal.home', {
            url: '/home',
            views: {
                'journal.contents': {
                    templateUrl: 'app/journal/views/home.html'
                }
            }
        }).state('journal.about', {
            url: '/about',
            views: {
                'journal.contents': {
                    templateUrl: 'app/journal/views/about.html'
                }
            }
        });
    }

})(angular);