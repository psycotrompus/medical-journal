(function(angular) {
    'use strict';

    var module = angular.module('journal');

    module.controller('MenuController', MenuController);

    MenuController.$inject = [ '$rootScope', 'AuthenticationService' ];

    function MenuController($rootScope, authenticationService) {
    	
    	var self = this;
    	
    	self.roles = {
    		'ADMIN': false,
    		'PUBLISHER': false,
    		'SUBSCRIBER': false,
    		'USER': false
    	};

        authenticationService.getAuthentication().then(function() {
        	var userDetails = $rootScope.authentication.userDetails;
        	userDetails.roles.forEach(function(role) {
        		self.roles[role] = true;
        	});
        });
    }

})(angular);