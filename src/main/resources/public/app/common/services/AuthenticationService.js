(function(angular) {
    'use strict';
    
    var module = angular.module('journal');
    
    module.service('AuthenticationService', AuthenticationService);

    AuthenticationService.$inject = [ '$rootScope', '$state', '$q', 'AuthenticationFactory' ];
    
    function AuthenticationService($rootScope, $state, $q, AuthenticationFactory) {
        
        var self = this;

        var roles = [];

        $rootScope.hasRole = function(subject) {
            return false;
        };

        self.hasRole = function(subject) {
            var credentials = $rootScope.authentication.userDetails;
            if (!credentials) {
                return false;
            }
            return credentials.roles.some(function(role) {
                if (role === subject) {
                    return true;
                }
            });
        };

        self.getAuthentication = function() {
            var promise;
            if ($rootScope.authentication) {
                promise = $q.when($rootScope.authentication);
            }
            else {
                promise = AuthenticationFactory.getAuthentication().$promise.then(function(auth) {
                    $rootScope.authentication = auth;
                    $rootScope.authenticated = auth.authenticated;
                    var userDetails = auth.userDetails;
                    $rootScope.fullname = userDetails.fullname;
                    roles = userDetails.roles;
                });
            }
            return promise.then(function() {
                $rootScope.hasRole = self.hasRole;
            }).catch(function(res) {
                if (res.status === 401) {
                    $state.go('sessionExpired');
                }
                else {
                    $state.go('accessDenied');
                }
                $rootScope.authentication = {
                    userDetails: {
                        username: '',
                        password: '',
                        roles: []
                    },
                    authenticated: false
                };
            });
        };
    }

    module.factory('AuthenticationFactory', AuthenticationFactory);

    AuthenticationFactory.$inject = [ '$resource' ];

    function AuthenticationFactory($resource) {

        var query = $resource('/authentication').get({});

        return {
            getAuthentication: function() {
                return query;
            }
        };
    }

})(angular);