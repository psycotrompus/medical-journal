(function(angular) {
    'use strict';
    
    var app = angular.module('journal');
    
    app.factory('NotyFactory', NotyFactory);
    
    NotyFactory.$inject = [ 'jQuery', '$window' ];

    function NotyFactory($, $window) {
        $.noty.defaults = {
            layout: 'bottomCenter',
            theme: 'defaultTheme', // or 'relax'
            dismissQueue: true, // If you want to use queue feature set this true
            template: '<div class="noty_message"><span class="noty_text"></span><div class="noty_close"></div></div>',
            animation: {
                open: 'animated fadeInDown', // or Animate.css class names like: 'animated bounceInLeft'
                close: 'animated fadeOutDown', // or Animate.css class names like: 'animated bounceOutLeft'
                easing: 'swing',
                speed: 500 // opening & closing animation speed
            },
            timeout: false, // delay for closing event. Set false for sticky notifications
            force: false, // adds notification to the beginning of queue when set to true
            modal: false,
            maxVisible: 5, // you can set max visible notification for dismissQueue true option,
            killer: false, // for close all notifications before show
            closeWith: [ 'click', 'hover' ], // ['click', 'button', 'hover', 'backdrop'] // backdrop click will close all notifications
            callback: {
                onShow: function() {},
                afterShow: function() {},
                onClose: function() {},
                afterClose: function() {},
                onCloseClick: function() {}
            },
            buttons: false // an array of buttons
        };
        return {
            alert: function(message) {
                return $window.noty({
                    type: 'alert',
                    text: message
                });
            },
            success: function(message) {
                return $window.noty({
                    type: 'success',
                    text: message
                });
            },
            error: function(message) {
                return $window.noty({
                    type: 'error',
                    text: message
                });
            },
            warning: function(message) {
                return $window.noty({
                    type: 'warning',
                    text: message
                });
            },
            info: function(message) {
                return $window.noty({
                    type: 'information',
                    text: message
                });
            },
            confirm: function(message, callbackYes, callbackNo, buttons) {
                return $window.noty({
                    type: 'confirm',
                    text: message,
                    modal: true,
                    buttons: buttons || [
                        {
                            addClass: 'btn btn-primary',
                            text: 'Yes',
                            onClick: function($noty) {
                                $noty.close();
                                callbackYes();
                            }
                        },
                        {
                            addClass: 'btn btn-default',
                            text: 'No',
                            onClick: function($noty) {
                                $noty.close();
                                if (callbackNo) {
                                    callbackNo();
                                }
                            }
                        }
                    ]
                });
            }
        };
    }
    
})(angular);