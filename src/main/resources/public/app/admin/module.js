(function(angular) {
    'use strict';

    angular.module('journal.admin', [ 'journal' ]);

})(angular);