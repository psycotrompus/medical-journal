(function(angular) {
    'use strict';
    
    var module = angular.module('journal.admin');

    // region UserService
    
    module.service('UserService', UserService);

    UserService.$inject = [ 'UserResource' ];
    
    function UserService(Users) {
        
        var self = this;

        var page = 0;

        var logError = function(res) {
            console.error(res.data.message);
            return false;
        };

        self.search = function(query) {
            return Users.query({
                'page': page,
                'query': query
            }).$promise.catch(logError);
        };

        self.nextPage = function(query) {
            page++;
            return self.search(query);
        };

        self.previousPage = function(query) {
            page--;
            return self.search(query);
        };

        self.saveUser = function(user) {
            return Users.save(user).$promise.catch(logError);
        };

        self.updateUser = function(user) {
            return Users.update(user).$promise.catch(logError);
        };

        self.getUser = function(userId) {
            return Users.get({ 'id' : userId }).$promise;
        };
    }

    // endregion

    // region UserResource

    module.factory('UserResource', UserResource);

    UserResource.$inject = [ '$resource' ];

    function UserResource($resource) {
        return $resource('/api/users/:id', { 'id' : '@userId' }, {
            update: {
                method: 'PUT'
            }
        });
    }

    // endregion
    
})(angular);