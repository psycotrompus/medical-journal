(function(angular) {
    'use strict';

    var module = angular.module('journal.admin');

    module.config(RouteConfig);

    RouteConfig.$inject = [ '$stateProvider' ];

    function RouteConfig($stateProvider) {
        $stateProvider.state('users', {
            url: '/users',
            abstract: true,
            defaultChild: 'users.list',
            views: {
                'header': {
                    templateUrl: 'app/common/views/menu.html',
                    controller: 'MenuController',
                    controllerAs: 'menuCtrl'
                },
                'contents': {
                    template: '<div ui-view="users.contents"></div>'
                }
            }
        }).state('users.list', {
            url: '',
            views: {
                'users.contents': {
                    templateUrl: 'app/admin/views/users.html',
                    controller: 'SearchUsersController',
                    controllerAs: 'userCtrl'
                }
            }
        }).state('users.add', {
            url: '/add',
            resolve: {
                'User': [ 'Moment',
                    function(moment) {
                        return {
                            username: '',
                            password: '',
                            lastName: '',
                            firstName: '',
                            middleName: '',
                            role: 3,
                            dateRegistered: moment(new Date()).format('YYYY-MM-DDThh:mm')
                        };
                    }
                ]
            },
            views: {
                'users.contents': {
                    templateUrl: 'app/admin/views/userForm.html',
                    controller: 'UserController',
                    controllerAs: 'userCtrl'
                }
            }
        }).state('users.edit', {
            url: '/:userId',
            resolve: {
                'User': [ '$stateParams', 'UserService',
                    function($stateParams, userService) {
                        return userService.getUser($stateParams.userId);
                    }
                ]
            },
            views: {
                'users.contents': {
                    templateUrl: 'app/admin/views/userForm.html',
                    controller: 'UserController',
                    controllerAs: 'userCtrl'
                }
            }
        });
    }

})(angular);