(function(angular) {
    'use strict';
    
    var module = angular.module('journal.admin');

    // region SearchUsersController
    
    module.controller('SearchUsersController', SearchUsersController);

    SearchUsersController.$inject = [ 'UserService' ];
    
    function SearchUsersController(userService) {

        var self = this;

        self.users = [];

        self.query = '';

        self.search = function() {
            userService.search(self.query).then(function(users) {
                self.users = users;
            });
        };

        self.search();

        self.nextPage = userService.nextPage;

        self.previousPage = userService.previousPage;
    }

    // endregion

    // region UserController

    module.controller('UserController', UserController);

    UserController.$inject = [ '$q', '$state', 'jQuery', 'UserService', 'User', 'NotyFactory' ];

    function UserController($q, $state, $, userService, user, Notify) {

        var self = this;

        var master = user;

        self.user = $.extend(true, {} ,user);

        self.saving = false;

        self.save = function(userForm) {
            self.saving = true;

            $q(function(resolve, reject) {
                if (!userForm.$valid) {
                    Notify.error('Invalid form.');
                    reject('Invalid form.');
                }
                resolve(userForm);
            }).then(function() {
                return userService.saveUser(self.user);
            }).then(function(success) {
                if (!success) {
                    Notify.error('Unable to save user.');
                }
                else {
                    Notify.success('User saved.');
                    $state.go('users.list');
                }
            }).finally(function() {
                self.saving = false;
            });
        };
    }

    // endregion
    
})(angular);