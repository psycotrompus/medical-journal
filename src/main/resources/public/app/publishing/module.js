(function(angular) {
    'use strict';

    var module = angular.module('journal.publishing', [ 'journal' ]);

    module.config(RouteConfig);

    RouteConfig.$inject = [ '$stateProvider' ];

    function RouteConfig($stateProvider) {
        $stateProvider.state('publishing', {
            url: '/publishing',
            abstract: true,
            defaultChild: 'publishing.publications',
            views: {
                'header': {
                    templateUrl: 'app/common/views/menu.html',
                    controller: 'MenuController',
                    controllerAs: 'menuCtrl'
                },
                'contents': {
                    template: '<div ui-view="publishing.contents"></div>'
                }
            }
        }).state('publishing.publications', {
            url: '/publications',
            views: {
                'publishing.contents': {
                    templateUrl: 'app/publishing/views/publications.html',
                    controller: 'PublicationsController',
                    controllerAs: 'pubsCtrl'
                }
            }
        }).state('publishing.publish', {
            url: '/publish',
            views: {
                'publishing.contents': {
                    templateUrl: 'app/publishing/views/publishForm.html',
                    controller: 'PublicationController',
                    controllerAs: 'pubCtrl'
                }
            }
        }).state('publishing.editJournal', {
            url: '/publish/:journalId',
            views: {
                'publishing.contents': {
                    templateUrl: 'app/publishing/views/publishForm.html',
                    controller: 'PublicationController',
                    controllerAs: 'pubCtrl'
                }
            },
            resolve: {
                'Publication': [
                    function() {
                        return {};
                    }
                ]
            }
        }).state('publishing.subscribe', {
            url: '/subscribe/:journalId',
            views: {
                'publishing.contents': {
                    templateUrl: 'app/publishing/views/subscription.html',
                    controller: 'SubscriptionController',
                    controllerAs: 'subCtrl'
                }
            },
            resolve: {
                'Publication': [ '$stateParams', 'PublishingService',
                    function($stateParams, publishingService) {
                        return publishingService.getJournal($stateParams.journalId);
                    }
                ]
            }
        }).state('publishing.subscriptions', {
            url: '/subscriptions',
            views: {
                'publishing.contents': {
                    templateUrl: 'app/publishing/views/subscriptions.html',
                    controller: 'SubscriptionsController',
                    controllerAs: 'subsCtrl'
                }
            }
        }).state('publishing.unsubscribe', {
            url: '/unsubscribe/:subscriberId',
            onEnter: [ '$stateParams', 'PublishingService',
                function($stateParams, publishingService) {
                    publishingService.unsubscribe($stateParams.subscriberId);
                }
            ],
            views: {
                'publishing.contents': {
                    controller: [ '$state',
                        function($state) {
                            $state.go('publishing.subscriptions');
                        }
                    ]
                }
            }
        });
    }

})(angular);