(function(angular) {
    'use strict';

    var module = angular.module('journal.publishing');

    module.controller('PublicationsController', PublicationsController);

    PublicationsController.$inject = [ 'PublishingService' ];

    function PublicationsController(publishingService) {

        var self = this;

        self.publications = [];

        self.query = '';

        self.search = function() {
            publishingService.searchJournals(self.query).then(function(list) {
                self.publications = list;
            });
        };

        self.search();
    }

})(angular);