(function(angular) {
    'use strict';
    
    var module = angular.module('journal.publishing');
    
    module.controller('PublicationController', PublicationController);

    PublicationController.$inject = [ '$scope', 'jQuery', '$timeout', '$state', 'PublishingService', 'NotyFactory' ];
    
    function PublicationController($scope, $, $timeout, $state, publishingService, Notify) {
        
        var self = this;

        self.journal = {
            title: '',
            subtitle: ''
        };
        
        self.publicationFile = '';
        
        publishingService.getPublishers().then(function(users) {
            var options = users.map(function(u) {
                return {
                    id: u.userId,
                    name: u.fullname
                };
            });
            $scope.dmsOptions = {
                title: 'Select your Publishers',
                filterPlaceHolder: 'Start typing to filter the publishers below.',
                labelAll: 'Available Publishers',
                labelSelected: 'Selected Publishers',
                helpMessage: ' Click items to transfer them between fields.',
                /* angular will use this to filter your lists */
                orderProperty: 'fullname',
                /* this contains the initial list of all items (i.e. the left side) */
                items: options,
                /* this list should be initialized as empty or with any pre-selected items */
                selectedItems: []
            };
        });

        self.publish = function() {
            var publisherIds = $scope.dmsOptions.selectedItems.map(function(u) {
                return u.id;
            });
            var publicationFile = $('#publicationFile')[0].files[0];
            publishingService.createPublication(self.journal, publicationFile, publisherIds).then(function(result) {
                Notify.success('Medical Journal created.');
                $timeout(function() {
                    $state.go('publishing.publications');
                }, 3000);
            });
        };
    }
    
})(angular);
