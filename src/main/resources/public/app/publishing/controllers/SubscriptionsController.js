(function(angular) {
    'use strict';

    var module = angular.module('journal.publishing');

    module.controller('SubscriptionsController', SubscriptionsController);

    SubscriptionsController.$inject = [ 'PublishingService' ];

    function SubscriptionsController(publishingService) {

        var self = this;

        self.subscriptions = [];

        publishingService.getSubscriptions().then(function(subscriptions) {
            self.subscriptions = subscriptions;
        });
    }

})(angular);