(function(angular) {
    'use strict';

    var module = angular.module('journal.publishing');

    module.controller('SubscriptionController', SubscriptionController);

    SubscriptionController.$inject = [ '$timeout', '$state', 'PublishingService', 'Publication', 'NotyFactory' ];

    function SubscriptionController($timeout, $state, publishingService, publication, Notify) {

        var self = this;

        self.journal = publication.journal;

        publishingService.subscribe(self.journal.journalId).then(function() {
            Notify.success('Subscribed to ' + self.journal.title);
            $timeout(function() {
                $state.go('publishing.subscriptions');
            }, 3000);
            self.success = true;
        }).catch(function() {
            Notify.error('Unable to subscribe.');
            self.success = false;
        });
    }

})(angular);