(function(angular) {
    'use strict';

    var module = angular.module('journal.publishing');

    // region PublishingService

    module.service('PublishingService', PublishingService);

    PublishingService.$inject = [ '$resource', '$http', '$q', 'JournalResource', 'SubscriptionResource' ];

    function PublishingService($resource, $http, $q, Journals, Subscriptions) {

        var self = this;

        var page = 0;

        self.getPublishers = function() {
            return Journals.query({ 'data' : 'publishers' }).$promise;
        };

        self.searchJournals = function(query) {
            return Journals.query({
                'page' : page,
                'query': query
            }).$promise;
        };

        self.nextPage = function(query) {
            page++;
            return self.searchJournals(query);
        };

        self.previousPage = function(query) {
            page--;
            return self.searchJournals(query);
        };

        self.createPublication = function(journal, uploadedFile, publisherIds) {
            return $q(function(resolve) {
                resolve({
                    journal: journal,
                    publisherIds: publisherIds
                });
            }).then(function(publication) {
                var formData = new FormData();
                formData.append('file', uploadedFile);
                formData.append('publication', angular.toJson(publication));
                return formData;
            }).then(function(formData) {
                return $http.post('/journals', formData, {
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                })
            });
        };

        self.getJournal = function(journalId) {
            return Journals.get({ 'data' : journalId }).$promise;
        };

        self.subscribe = function(journalId) {
            return Subscriptions.save({ 'subId' : journalId }, {}).$promise;
        };

        self.getSubscriptions = function() {
            return Subscriptions.query({}).$promise;
        };

        self.unsubscribe = function(subscriberId) {
            return Subscriptions.delete({ 'subId': subscriberId }).$promise;
        };
    }

    // endregion

    // region SubscriptionResource

    module.factory('SubscriptionResource', SubscriptionResource);

    SubscriptionResource.$inject = [ '$resource' ];

    function SubscriptionResource($resource) {
        return $resource('/subscriptions/:subId');
    }

    // endregion

    // region JournalResource

    module.factory('JournalResource', JournalResource);

    JournalResource.$inject = [ '$resource' ];

    function JournalResource($resource) {
        return $resource('/journals/:data');
    }

    // endregion

})(angular);