(function(angular) {
    'use strict';

    var module = angular.module('journal.security');

    module.service('SecurityService', SecurityService);

    SecurityService.$inject = [ '$rootScope', '$resource' ];

    function SecurityService($rootScope, $resource) {

        var self = this;

        var loginService = $resource('security/login', null, {
        	login: {
        		method: 'POST'
        	}
        });
        
        var logoutService = $resource('security/logout', null, {
        	logout: {
        		method: 'POST'
        	}
        });

        self.login = function(credentials) {
            return loginService.login(credentials).$promise.then(function(result) {
                var authentication = {
                    userDetails: result.userDetails,
                    authenticated: result.authenticated
                };
                $rootScope.authentication = authentication;
                $rootScope.authenticated = result.authenticated;
                $rootScope.fullname = result.userDetails.fullname;
                return result;
            });
        };

        self.logout = function() {
            return logoutService.logout().$promise.then(function() {
                $rootScope.authentication = {
                    userDetails: {
                        username: '',
                        password: '',
                        roles: []
                    },
                    authenticated: false
                };
                $rootScope.authenticated = false;
                delete $rootScope.fullname;
            });
        };
    }

})(angular);