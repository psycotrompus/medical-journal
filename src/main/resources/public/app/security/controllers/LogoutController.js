(function(angular) {
    'use strict';
    
    var module = angular.module('journal.security');
    
    module.controller('LogoutController', LogoutController);

    LogoutController.$inject = [ '$timeout', '$state', 'SecurityService' ];
    
    function LogoutController($timeout, $state, securityService) {

        securityService.logout().then(function() {
            $timeout(function() {
                $state.go('journal.home');
            }, 3000);
        });
    }
    
})(angular);