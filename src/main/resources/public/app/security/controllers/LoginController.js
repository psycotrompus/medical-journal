(function(angular) {
    'use strict';

    var module = angular.module('journal.security');

    module.controller('LoginController', LoginController);

    LoginController.$inject = [ '$scope', '$window', '$timeout', 'NotyFactory', 'SecurityService' ];

    function LoginController($scope, $window, $timeout, Notify, securityService) {
        
        var self = this;

        $scope.credentials = {
            username: '',
            password: ''
        };
        
        self.login = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.loading = true;
            securityService.login($scope.credentials).then(function() {
                Notify.success('Login successfully. Please wait while we refresh the page...');
                $timeout(function() {
                    $window.location.href = '/';
                }, 3000);
            }).catch(function(e) {
                if (e.status === 401) {
                    Notify.error('Invalid username/password.');
                }
                else {
                    Notify.error(e.data.message || 'Authentication failure.');
                }
            }).finally(function() {
                $scope.loading = false;
            });
        };
    }

})(angular);