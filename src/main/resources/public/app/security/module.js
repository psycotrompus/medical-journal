(function(angular) {
    'use strict';

    var module = angular.module('journal.security', [ 'journal' ]);

    module.config(RouteConfig);

    RouteConfig.$inject = [ '$stateProvider' ];

    function RouteConfig($stateProvider) {

        $stateProvider.state('accessDenied', {
            url: '/forbidden',
            views: {
                'header': {
                    templateUrl: 'app/common/views/menu.html',
                    controller: 'MenuController',
                    controllerAs: 'menuCtrl'
                },
                'contents': {
                    templateUrl: 'app/security/views/accessDenied.html'
                }
            }
        }).state('sessionExpired', {
            url: '/expired',
            views: {
                'header': {
                    templateUrl: 'app/common/views/menu.html',
                    controller: 'MenuController',
                    controllerAs: 'menuCtrl'
                },
                'contents': {
                    templateUrl: 'app/security/views/sessionExpired.html',
                    controller: [ '$timeout', '$state',
                        function($timeout, $state) {
                            $timeout(function() {
                                $state.go('security.login');
                            }, 3000);
                        }
                    ]
                }
            }
        });

        $stateProvider.state('security', {
            url: '/security',
            abstract: true,
            defaultChild: 'security.login',
            views: {
                'header': {
                    templateUrl: 'app/common/views/menu.html',
                    controller: 'MenuController',
                    controllerAs: 'menuCtrl'
                },
                'contents': {
                    template: '<div ui-view="security.contents"></div>'
                }
            }
        }).state('security.login', {
            url: '/login',
            views: {
                'security.contents': {
                    templateUrl: 'app/security/views/login.html',
                    controller: 'LoginController',
                    controllerAs: 'loginCtrl'
                }
            }
        }).state('security.logout', {
            url: '/logout',
            views: {
                'security.contents': {
                    templateUrl: 'app/security/views/logout.html',
                    controller: 'LogoutController',
                    controllerAs: 'logoutCtrl'
                }
            }
        });
    }

})(angular);