(function(angular) {
    'use strict';

    var app = angular.module('journal', [
        'ngResource', 'ngAnimate', 'ngSanitize', 'ui.router', 'ui.bootstrap', 'dualmultiselect',
        'journal.security',
        'journal.admin',
        'journal.publishing'
    ]);

    app.config([ '$locationProvider',
        function($locationProvider) {
            $locationProvider.html5Mode(true);
        }
    ]);

    app.factory('jQuery', [ '$window',
        function($window) {
            return $window.jQuery;
        }
    ]);

    app.factory('Moment', [ '$window',
        function($window) {
            return $window.moment;
        }
    ]);
    
    app.factory('RestConfig', function() {
        return {
            username: 'admin',
            password: 'cGFzc3dvcmQ='
        };
    });

    app.run([ '$http', 'RestConfig',
        function($http, RestConfig) {
    		var authorization = 'Basic ' + btoa(RestConfig.username + ':' + atob(RestConfig.password));
            $http.defaults.headers.common.Authorization = authorization;
        }
    ]);

})(angular);