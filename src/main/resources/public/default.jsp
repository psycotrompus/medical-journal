<!DOCTYPE html>
<html lang="en">
  <head>
    <base href="/"/>
    <meta charset="UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>Medical Journal Application</title>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous"/>
    <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet"/>
    <link href="//code.jquery.com/ui/1.11.4/themes/pepper-grinder/jquery-ui.css" rel="stylesheet"/>

    <link href="/libs/dualmultiselect/dualmultiselect.css" rel="stylesheet"/>

    <link href="/css/app.css" rel="stylesheet"/>
    <link href="/css/animate.css" rel="stylesheet"/>

    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/blueimp-file-upload/9.12.5/js/vendor/jquery.ui.widget.min.js"></script>

  </head>
  <body ng-app="journal" ng-cloak="" role="document">

    <div class="container">
      <div ui-view="header"></div>
      <div ui-view="contents"></div>
    </div>

    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"
            integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous">
    </script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.14.1/moment.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.7/angular.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.7/angular-animate.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.7/angular-resource.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.7/angular-sanitize.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/angular-ui-router/0.3.1/angular-ui-router.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/1.3.3/ui-bootstrap-tpls.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-noty/2.3.8/packaged/jquery.noty.packaged.min.js"></script>
    <script src="/libs/dualmultiselect/dualmultiselect.js"></script>

    <script src="/app/app.js"></script>
    <script src="/app/common/noty.js"></script>
    <script src="/app/common/services/AuthenticationService.js"></script>
    <script src="/app/common/controllers/MenuController.js"></script>

    <script src="/app/journal/module.js"></script>

    <script src="/app/security/module.js"></script>
    <script src="/app/security/services/SecurityService.js"></script>
    <script src="/app/security/controllers/LoginController.js"></script>
    <script src="/app/security/controllers/LogoutController.js"></script>

    <script src="/app/admin/module.js"></script>
    <script src="/app/admin/services/AdminService.js"></script>
    <script src="/app/admin/controllers/UserController.js"></script>
    <script src="/app/admin/RoutesConfig.js"></script>

    <script src="/app/publishing/module.js"></script>
    <script src="/app/publishing/services/PublishingService.js"></script>
    <script src="/app/publishing/controllers/PublicationsController.js"></script>
    <script src="/app/publishing/controllers/PublicationController.js"></script>
    <script src="/app/publishing/controllers/SubscriptionController.js"></script>
    <script src="/app/publishing/controllers/SubscriptionsController.js"></script>

  </body>
</html>