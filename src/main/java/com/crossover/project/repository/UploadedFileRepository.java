package com.crossover.project.repository;

import com.crossover.project.entity.UploadedFile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.RepositoryDefinition;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by ej on 7/16/16.
 */
@Repository
@RepositoryDefinition(domainClass = UploadedFile.class, idClass = Long.class)
public interface UploadedFileRepository extends JpaRepository<UploadedFile, Long> {

    Page<UploadedFile> findByUploadedBy(@Param("userId") long userId, Pageable pageable);
}
