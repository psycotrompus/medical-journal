package com.crossover.project.repository;

import com.crossover.project.entity.Journal;
import com.crossover.project.entity.Publisher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.RepositoryDefinition;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by ej on 7/16/16.
 */
@Repository
@RepositoryDefinition(domainClass = Publisher.class, idClass = Long.class)
public interface PublisherRepository extends JpaRepository<Publisher, Long> {

    List<Publisher> findByJournal(Journal journal);
}
