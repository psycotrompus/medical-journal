package com.crossover.project.repository;

import com.crossover.project.entity.Journal;
import com.crossover.project.entity.Subscriber;
import com.crossover.project.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.RepositoryDefinition;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by ej on 7/16/16.
 */
@Repository
@RepositoryDefinition(domainClass = Subscriber.class, idClass = Long.class)
public interface SubscriberRepository extends JpaRepository<Subscriber, Long> {

    List<Subscriber> findByUser(User user);

    List<Subscriber> findByJournal(Journal journal);
}
