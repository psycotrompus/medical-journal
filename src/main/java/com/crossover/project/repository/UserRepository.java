package com.crossover.project.repository;

import com.crossover.project.entity.Roles;
import com.crossover.project.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.RepositoryDefinition;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Created by ej on 7/16/16.
 */
@Repository
@RepositoryDefinition(domainClass = User.class, idClass = Long.class)
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByUsername(String username);

    @Query("select u from User u where u.username like ?1% or u.firstName like ?1% or u.lastName like ?1% order by u.lastName, u.firstName")
    Page<User> searchUsers(String query, Pageable pageable);

    List<User> findByRole(Roles role);

    Page<User> findByRole(Roles role, Pageable pageable);
}
