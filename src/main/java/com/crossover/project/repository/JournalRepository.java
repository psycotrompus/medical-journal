package com.crossover.project.repository;

import com.crossover.project.entity.Journal;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.RepositoryDefinition;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by ej on 7/16/16.
 */
@Repository
@RepositoryDefinition(domainClass = Journal.class, idClass = Long.class)
public interface JournalRepository extends JpaRepository<Journal, Long> {

    @Query("select j from Journal j where j.title like :query or j.subtitle like :query")
    Page<Journal> searchJournals(@Param("query") String query, Pageable pageable);
}
