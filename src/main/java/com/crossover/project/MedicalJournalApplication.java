package com.crossover.project;

import org.jasypt.util.password.PasswordEncryptor;
import org.jasypt.util.password.StrongPasswordEncryptor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class MedicalJournalApplication {

	@Bean
	public PasswordEncryptor passwordEncryptor() {
		return new StrongPasswordEncryptor();
	}

	public static void main(String[] args) {
		SpringApplication.run(MedicalJournalApplication.class, args);
	}
}
