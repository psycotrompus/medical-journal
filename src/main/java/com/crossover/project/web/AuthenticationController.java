package com.crossover.project.web;

import com.crossover.project.service.AuthenticationException;
import com.crossover.project.service.SessionExpiredException;
import com.crossover.project.service.UserDto;
import com.crossover.project.util.DefaultUserSessionAccessor;
import com.crossover.project.util.UserSessionAccessor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Optional;

/**
 * Created by ej on 7/18/16.
 */
@RestController
@RequestMapping("/authentication")
public class AuthenticationController {

    @RequestMapping(produces = { MediaType.APPLICATION_JSON_VALUE })
    public LoginResponseBean get(HttpServletRequest httpRequest) {
        HttpSession httpSession = httpRequest.getSession(false);
        UserSessionAccessor userSessionAccessor = new DefaultUserSessionAccessor(httpSession);
        if (userSessionAccessor.hasExpired()) {
            throw new SessionExpiredException();
        }
        Optional<UserDto> optional = userSessionAccessor.getUser();
        if (!optional.isPresent()) {
            throw new AuthenticationException("User not authenticated.");
        }
        UserDto user = optional.get();
        return LoginResponseBean.builder()
                .authenticated(true)
                .userDetails(user)
                .build();
    }

    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ExceptionHandler({ AuthenticationException.class })
    public LoginResponseBean handleAuthenticationException(AuthenticationException exception) {
        return LoginResponseBean.builder()
                .authenticated(false)
                .userDetails(UserDto.empty())
                .build();
    }

    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler({ SessionExpiredException.class })
    public LoginResponseBean handleSessionExpired(SessionExpiredException exception) {
        return LoginResponseBean.builder()
                .authenticated(false)
                .userDetails(UserDto.empty())
                .build();
    }
}
