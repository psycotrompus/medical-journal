package com.crossover.project.web;

import com.crossover.project.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@RestController
@RequestMapping("/security/login")
public class LoginController {

    private SecurityService securityService;

    @Autowired
    public void setSecurityService(SecurityService securityService) {
        this.securityService = securityService;
    }

    @RequestMapping(method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE })
    public LoginResponseBean login(@RequestBody LoginRequestBean request, HttpServletRequest httpRequest) {
        HttpSession httpSession = httpRequest.getSession();
        LoginRequestDto requestDto = LoginRequestDto.builder()
                .username(request.getUsername())
                .password(request.getPassword())
                .sessionId(httpSession.getId())
                .build();
        LoginResponseDto responseDto = securityService.login(requestDto);
        httpSession.setAttribute(UserDto.USER_ATTRIBUTE, responseDto.getUser());
        return LoginResponseBean.builder()
                .userDetails(responseDto.getUser())
                .authenticated(true)
                .build();
    }

    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler({ AuthenticationException.class })
    public LoginResponseBean handleBadCredentials(AuthenticationException exception) {
        return LoginResponseBean.builder()
                .authenticated(false)
                .build();
    }
}
