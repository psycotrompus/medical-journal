package com.crossover.project.web;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LoginRequestBean implements Serializable {

    private static final long serialVersionUID = -8259303071159470644L;

    private String username;

    private String password;

    private Map<String, String> data;
}
