package com.crossover.project.web;

import com.crossover.project.entity.Roles;
import com.crossover.project.entity.UploadedFile;
import com.crossover.project.entity.User;
import com.crossover.project.service.AccessDeniedException;
import com.crossover.project.service.JournalDto;
import com.crossover.project.service.PublishingService;
import com.crossover.project.service.UserDto;
import com.crossover.project.util.DefaultFileUtil;
import com.crossover.project.util.DefaultUserSessionAccessor;
import com.crossover.project.util.FileUtil;
import com.crossover.project.util.UserSessionAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

@RestController
@RequestMapping("/journals")
public class JournalRestController {

    public static final String FILE_PARAM = "file";

    private PublishingService publishingService;

    @Autowired
    public void setPublishingService(PublishingService publishingService) {
        this.publishingService = publishingService;
    }

    private ObjectMapper objectMapper;

    @Autowired
    public void setObjectMapper(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Secured(Roles.USER)
    @RequestMapping
    public List<PublishingBean> searchJournals(@RequestParam(required = false) Integer page,
                                        @RequestParam(required = false) String query) {
        return publishingService.searchJournals(page, query).parallelStream()
                .map(dto -> {
                    PublishingBean.PublishingBeanBuilder builder = PublishingBean.builder()
                            .journal(dto)
                            .uploadedFile(dto.getUploadedFile());
                    dto.getPublisherIds().parallelStream()
                            .forEach(builder::publisherId);
                    dto.getSubscriberIds().parallelStream()
                            .forEach(builder::subscriberId);
                    return builder.build();
                }).collect(toList());
    }

    @RequestMapping("/publishers")
    public List<User> getPublishers() {
        return publishingService.getAllPublishers();
    }

    @Secured(Roles.PUBLISHER)
    @RequestMapping(method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
    public JournalDto create(@RequestPart(FILE_PARAM) MultipartFile multipartFile,
                             @RequestParam("publication") String publicationStr, HttpServletRequest request) {
        FileUtil fileUtil = new DefaultFileUtil();
        UploadedFile uploadedFile = fileUtil.createUploadedFile(multipartFile);
        HttpSession httpSession = request.getSession(false);
        UserSessionAccessor userSessionAccessor = new DefaultUserSessionAccessor(httpSession);
        userSessionAccessor.checkExpired();
        Optional<UserDto> optional = userSessionAccessor.getUser();
        if (!optional.isPresent()) {
            throw new AccessDeniedException();
        }
        UserDto user = optional.get();
        PublishingBean publication;
        try {
            publication = objectMapper.readValue(publicationStr, PublishingBean.class);
        }
        catch (IOException e) {
            throw new RuntimeException("Invalid Journal data.");
        }
        publishingService.saveJournal(publication.getJournal(), uploadedFile, user.getUserId(),
                publication.getPublisherIds());
        return publication.getJournal();
    }

    @RequestMapping("/{journalId}")
    public PublishingBean getJournal(@PathVariable long journalId) {
        JournalDto dto = publishingService.getJournal(journalId);
        PublishingBean.PublishingBeanBuilder builder = PublishingBean.builder()
                .journal(dto)
                .uploadedFile(dto.getUploadedFile());
        dto.getPublisherIds().parallelStream()
                .forEach(builder::publisherId);
        dto.getSubscriberIds().parallelStream()
                .forEach(builder::subscriberId);
        return builder.build();
    }
}
