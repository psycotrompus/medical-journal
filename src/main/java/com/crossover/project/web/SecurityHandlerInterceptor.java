package com.crossover.project.web;

import com.crossover.project.entity.Roles;
import com.crossover.project.service.AccessDeniedException;
import com.crossover.project.service.UserDto;
import com.crossover.project.util.DefaultUserSessionAccessor;
import com.crossover.project.util.UserSessionAccessor;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Optional;

/**
 * Created by ej on 7/18/16.
 */
@Component
public class SecurityHandlerInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HandlerMethod handlerMethod = (HandlerMethod) handler;
        Method method = handlerMethod.getMethod();
        Secured secured = AnnotationUtils.findAnnotation(method, Secured.class);
        if (secured == null) {
            return true;
        }
        HttpSession httpSession = request.getSession(false);
        if (httpSession == null) {
            return true;
        }
        UserSessionAccessor userSessionAccessor = new DefaultUserSessionAccessor(httpSession);
        Roles methodRole = secured.value();
        Optional<UserDto> optional = userSessionAccessor.getUser();
        if (!optional.isPresent()) {
            throw new AccessDeniedException();
        }
        List<String> userRoles = optional.get().getRoles();
        Optional<String> foundRole = userRoles.parallelStream()
                .filter(userRole -> methodRole.toString().equals(userRole))
                .findFirst();
        if (!foundRole.isPresent()) {
            throw new AccessDeniedException();
        }
        return true;
    }
}
