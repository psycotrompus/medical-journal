package com.crossover.project.web;

import com.crossover.project.entity.User;
import com.crossover.project.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by ejlayco on 7/19/16.
 */
@RestController
@RequestMapping("/api/users")
public class UserRestController {

    private AdminService adminService;

    @Autowired
    public void setAdminService(AdminService adminService) {
        this.adminService = adminService;
    }

    @RequestMapping
    public List<User> search(@RequestParam(required = false) Integer page, @RequestParam(required = false) String query) {
        return adminService.searchUsers(page, query);
    }

    @RequestMapping("/{userId}")
    public User edit(@PathVariable long userId) {
        return adminService.getUser(userId);
    }

    @RequestMapping(method = RequestMethod.POST)
    public User save(@Valid @RequestBody User user) {
        adminService.saveUser(user);
        return user;
    }

    @RequestMapping(value = "/{userId}", method = RequestMethod.POST)
    public User update(@PathVariable long userId, @Valid @RequestBody User user) {
        user.setUserId(userId);
        return adminService.updateUser(user);
    }

    @RequestMapping(value = "/{userId}", method = RequestMethod.DELETE)
    public boolean delete(@PathVariable Long userId) {
        return adminService.deleteUser(userId);
    }
}
