package com.crossover.project.web;

import com.crossover.project.service.SecurityService;
import com.crossover.project.service.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

@RestController
@RequestMapping("/security/logout")
public class LogoutController {

	private SecurityService securityService;

	@Autowired
	public void setSecurityService(SecurityService securityService) {
		this.securityService = securityService;
	}

	@RequestMapping(method = { RequestMethod.POST })
	public Boolean logout(HttpSession httpSession) {
		UserDto userDto = (UserDto) httpSession.getAttribute(UserDto.USER_ATTRIBUTE);
		securityService.logout(userDto.getUserId());
		httpSession.invalidate();
		return true;
	}
}
