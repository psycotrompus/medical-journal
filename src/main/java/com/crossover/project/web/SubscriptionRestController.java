package com.crossover.project.web;

import com.crossover.project.entity.Roles;
import com.crossover.project.entity.Subscriber;
import com.crossover.project.service.AccessDeniedException;
import com.crossover.project.service.PublishingService;
import com.crossover.project.service.UserDto;
import com.crossover.project.util.DefaultUserSessionAccessor;
import com.crossover.project.util.UserSessionAccessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Optional;

/**
 * Created by ejlayco on 7/20/16.
 */
@RestController
@RequestMapping("/subscriptions")
public class SubscriptionRestController {

    private PublishingService publishingService;

    @Autowired
    public void setPublishingService(PublishingService publishingService) {
        this.publishingService = publishingService;
    }

    @Secured(Roles.SUBSCRIBER)
    @RequestMapping(method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
    public List<Subscriber> getSubscriptions(HttpServletRequest request) {
        HttpSession httpSession = request.getSession(false);
        UserSessionAccessor userSessionAccessor = new DefaultUserSessionAccessor(httpSession);
        userSessionAccessor.checkExpired();
        Optional<UserDto> optional = userSessionAccessor.getUser();
        if (!optional.isPresent()) {
            throw new AccessDeniedException();
        }
        UserDto user = optional.get();
        return publishingService.getSubscriptions(user.getUserId());
    }

    @Secured(Roles.SUBSCRIBER)
    @RequestMapping(value = "/{journalId}", method = RequestMethod.POST)
    public Subscriber subscribe(@PathVariable long journalId, HttpServletRequest request) {
        HttpSession httpSession = request.getSession(false);
        UserSessionAccessor userSessionAccessor = new DefaultUserSessionAccessor(httpSession);
        userSessionAccessor.checkExpired();
        Optional<UserDto> optional = userSessionAccessor.getUser();
        if (!optional.isPresent()) {
            throw new AccessDeniedException();
        }
        UserDto user = optional.get();
        return publishingService.subscribe(user.getUserId(), journalId);
    }

    @Secured(Roles.SUBSCRIBER)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "/{subscriberId}", method = RequestMethod.DELETE)
    public void unsubscribe(@PathVariable long subscriberId) {
        publishingService.unsubscribe(subscriberId);
    }
}
