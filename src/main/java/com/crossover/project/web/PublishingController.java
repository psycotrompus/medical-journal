package com.crossover.project.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class PublishingController {

    @RequestMapping({ "/publishing/**" })
    public String index() {
        return "default";
    }
}
