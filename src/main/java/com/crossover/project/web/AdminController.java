package com.crossover.project.web;

import com.crossover.project.entity.Roles;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AdminController {

	@Secured(Roles.ADMIN)
	@RequestMapping({ "/users", "/users/**" })
	public String index() {
		return "default";
	}
}
