package com.crossover.project.web;

import com.crossover.project.service.UserDto;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LoginResponseBean implements Serializable {

    private static final long serialVersionUID = -7003673924676190894L;

    private UserDto userDetails;

    private boolean authenticated;
}
