package com.crossover.project.web;

import com.crossover.project.entity.UploadedFile;
import com.crossover.project.service.JournalDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Singular;

import java.io.Serializable;
import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PublishingBean implements Serializable {

    private static final long serialVersionUID = -5455690731802374318L;

    private JournalDto journal;

    private UploadedFile uploadedFile;

    @Singular
    private Set<Long> publisherIds;

    @Singular
    private Set<Long> subscriberIds;
}
