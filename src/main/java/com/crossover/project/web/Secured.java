package com.crossover.project.web;

import com.crossover.project.entity.Roles;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 *
 * Created by ej on 7/18/16.
 */
@Documented
@Retention(RUNTIME)
@Target({ METHOD })
public @interface Secured {

    Roles value();
}
