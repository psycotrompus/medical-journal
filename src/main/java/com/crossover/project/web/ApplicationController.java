package com.crossover.project.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ApplicationController {

    public static final String INDEX_VIEW = "default";

    @RequestMapping(value = { "/", "/home", "/about", "/security/**", "/expired", "/forbidden" })
    public String index() {
        return INDEX_VIEW;
    }
}
