package com.crossover.project.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "publishers")
public class Publisher implements Serializable {

    private static final long serialVersionUID = 8516777289775829308L;

    @Id
    @GeneratedValue
    @Column(nullable = false, unique = true)
    private long publisherId;

    @Column(nullable = false, length = 200)
    @Size(min = 1, max = 200)
    private String name;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    @NotNull
    private User user;

    @ManyToOne
    @JoinColumn(name = "journal_id", nullable = false)
    @NotNull
    private Journal journal;
}
