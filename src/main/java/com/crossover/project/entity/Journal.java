package com.crossover.project.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "journals")
public class Journal implements Serializable {

    private static final long serialVersionUID = -7864445468974782015L;

    @Id
    @GeneratedValue
    @Column(nullable = false, unique = true)
    private long journalId;

    @Column(nullable = false, length = 200)
    @NotNull
    @Size(min = 1, max = 200)
    private String title;

    @Column(length = 200)
    @Size(max = 200)
    private String subtitle;

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @NotNull
    private Date datePublished;

    @ManyToOne
    @JoinColumn(name = "uploaded_file_id", nullable = false)
    @NotNull
    private UploadedFile uploadedFile;
}
