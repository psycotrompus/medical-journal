package com.crossover.project.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

import static org.apache.commons.lang3.StringUtils.isBlank;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "users", uniqueConstraints = {
        @UniqueConstraint(name = "user_username_uk", columnNames = { "username" }),
        @UniqueConstraint(name = "user_password_uk", columnNames = { "password" })
})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class User implements Serializable {

    private static final long serialVersionUID = -5881529910432269554L;

    @Id
    @GeneratedValue
    @Column(nullable = false)
    private long userId;

    @Column(nullable = false, unique = true, length = 20)
    @NotNull
    @Size(min = 1, max = 20)
    private String username;

    @Column(nullable = false, unique = true, length = 200)
    @NotNull
    @Size(min = 1, max = 200)
    private String password;

    @Column(nullable = false, length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    private String lastName;

    @Column(nullable = false, length = 50)
    @NotNull
    @Size(min = 1, max = 50)
    private String firstName;

    @Column(length = 50)
    @Size(max = 50)
    private String middleName;

    @Enumerated
    @Column(nullable = false)
    @NotNull
    private Roles role;

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @NotNull
    @Past
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    private Date dateRegistered;

    @Column(length = 50)
    private String sessionId;

    public String getFullname() {
        StringBuilder sb = new StringBuilder();
        sb.append(lastName).append(", ").append(firstName);
        if (!isBlank(middleName)) {
            sb.append(" ").append(middleName.charAt(0) + ".");
        }
        return sb.toString();
    }
}
