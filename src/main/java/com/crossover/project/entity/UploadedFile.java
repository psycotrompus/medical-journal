package com.crossover.project.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "uploaded_files", uniqueConstraints = {
        @UniqueConstraint(name = "file_stored_filename_uk", columnNames = { "storedFilename" })
})
public class UploadedFile implements Serializable {

    private static final long serialVersionUID = 6150862949811424876L;

    @Id
    @GeneratedValue
    @Column(nullable = false, unique = true)
    private long fileId;

    @Column(nullable = false, unique = true, length = 200)
    @NotNull
    @Size(min = 1, max = 200)
    private String storedFilename;

    @Column(nullable = false, length = 200)
    @NotNull
    @Size(min = 1, max = 200)
    private String originalFilename;

    @ManyToOne
    @JoinColumn(name = "uploaded_by", nullable = false)
    @NotNull
    private User uploadedBy;

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUploaded;
}
