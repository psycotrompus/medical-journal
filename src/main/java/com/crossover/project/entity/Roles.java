package com.crossover.project.entity;

/**
 * Created by ej on 7/15/16.
 */
public enum Roles {

    ADMIN,
    PUBLISHER,
    SUBSCRIBER,
    USER

    ;

    @Override
    public String toString() {
        return name();
    }
}
