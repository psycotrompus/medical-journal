package com.crossover.project.service;

import lombok.*;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;
import java.util.List;

import static java.util.Collections.emptyList;

/**
 * Created by ej on 7/18/16.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class UserDto implements Serializable {

    public static final String USER_ATTRIBUTE = "__user_details__";

    private static final long serialVersionUID = -777147993036672769L;

    public static UserDto empty() {
        return UserDto.builder()
                .username("")
                .roles(emptyList())
                .build();
    }

    private long userId;

    private String username;

    private String fullname;

    @Singular
    private List<String> roles;

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                .append("userId", userId)
                .append("username", username)
                .append("fullname", fullname)
                .append("roles", roles)
                .toString();
    }
}
