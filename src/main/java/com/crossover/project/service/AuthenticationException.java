package com.crossover.project.service;

/**
 * Created by ej on 7/18/16.
 */
public class AuthenticationException extends RuntimeException {

    private static final long serialVersionUID = -989077781605744514L;

    public AuthenticationException(String message) {
        super(message);
    }
}
