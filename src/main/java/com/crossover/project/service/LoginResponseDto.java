package com.crossover.project.service;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LoginResponseDto implements Serializable {

	private static final long serialVersionUID = 820068412643677768L;
	
	private UserDto user;
}
