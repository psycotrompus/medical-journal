package com.crossover.project.service;

import com.crossover.project.entity.Roles;
import com.crossover.project.entity.User;
import com.crossover.project.repository.UserRepository;
import org.jasypt.util.password.PasswordEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static java.util.Arrays.asList;

@Service
class DefaultSecurityService implements SecurityService {
	
	private UserRepository userRepository;
	
	@Autowired
	public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    private PasswordEncryptor passwordEncryptor;

    @Autowired
    public void setPasswordEncryptor(PasswordEncryptor passwordEncryptor) {
        this.passwordEncryptor = passwordEncryptor;
    }

    @Transactional
    public LoginResponseDto login(LoginRequestDto loginRequest) throws AuthenticationException {
        Optional<User> optional = userRepository.findByUsername(loginRequest.getUsername());
		if (!optional.isPresent()) {
			throw new AuthenticationException(loginRequest.getUsername());
		}
		User user = optional.get();
		if (!passwordEncryptor.checkPassword(loginRequest.getPassword(), user.getPassword())) {
			throw new AuthenticationException(loginRequest.getUsername());
		}
        user.setSessionId(loginRequest.getSessionId());
        userRepository.save(user);
        UserDto dto = UserDto.builder()
                .userId(user.getUserId())
                .username(user.getUsername())
                .fullname(user.getFullname())
                .roles(asList(Roles.USER.toString(), user.getRole().toString()))
                .build();
        return LoginResponseDto.builder()
                .user(dto)
                .build();
	}

    @Transactional
	public void logout(long userId) {
        User user = userRepository.findOne(userId);
        user.setSessionId(null);
	}
}
