package com.crossover.project.service;

import com.crossover.project.entity.Subscriber;
import com.crossover.project.entity.UploadedFile;
import com.crossover.project.entity.User;

import java.util.List;
import java.util.Set;

/**
 * Created by ej on 7/19/16.
 */
public interface PublishingService {

    List<JournalDto> searchJournals(Integer page, String query);

    JournalDto getJournal(long journalId);

    void saveJournal(JournalDto journalDto, UploadedFile uploadedFile, Long authorUserId, Set<Long> publisherIds);

    List<User> getAllPublishers();

    Subscriber subscribe(long userId, long journalId);

    List<Subscriber> getSubscriptions(long userId);

    void unsubscribe(long subscriberId);
}
