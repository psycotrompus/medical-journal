package com.crossover.project.service;

/**
 * Created by ejlayco on 7/19/16.
 */
public class SessionExpiredException extends RuntimeException {

    private static final long serialVersionUID = 1868226332885204741L;

    public SessionExpiredException() {
        this("Session has expired.");
    }

    public SessionExpiredException(String message) {
        super(message);
    }
}
