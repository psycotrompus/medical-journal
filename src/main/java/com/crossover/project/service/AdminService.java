package com.crossover.project.service;

import java.util.List;

import com.crossover.project.entity.User;

public interface AdminService {
	
	List<User> searchUsers(Integer page, String query);

	User getUser(long userId);
	
	void saveUser(User user);
	
	User updateUser(User user);
	
	boolean deleteUser(long userId);
}
