package com.crossover.project.service;

public interface SecurityService {
	
	LoginResponseDto login(LoginRequestDto loginRequest) throws AuthenticationException;
	
	void logout(long userId);
}
