package com.crossover.project.service;

import com.crossover.project.entity.Journal;
import com.crossover.project.entity.Publisher;
import com.crossover.project.entity.Roles;
import com.crossover.project.entity.Subscriber;
import com.crossover.project.entity.UploadedFile;
import com.crossover.project.entity.User;
import com.crossover.project.repository.JournalRepository;
import com.crossover.project.repository.PublisherRepository;
import com.crossover.project.repository.SubscriberRepository;
import com.crossover.project.repository.UploadedFileRepository;
import com.crossover.project.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Set;

import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.StringUtils.isBlank;

@Service
public class DefaultPublishingService implements PublishingService {

    private UserRepository userRepository;

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    private JournalRepository journalRepository;

    @Autowired
    public void setJournalRepository(JournalRepository journalRepository) {
        this.journalRepository = journalRepository;
    }

    private UploadedFileRepository uploadedFileRepository;

    @Autowired
    public void setUploadedFileRepository(UploadedFileRepository uploadedFileRepository) {
        this.uploadedFileRepository = uploadedFileRepository;
    }

    private PublisherRepository publisherRepository;

    @Autowired
    public void setPublisherRepository(PublisherRepository publisherRepository) {
        this.publisherRepository = publisherRepository;
    }

    private SubscriberRepository subscriberRepository;

    @Autowired
    public void setSubscriberRepository(SubscriberRepository subscriberRepository) {
        this.subscriberRepository = subscriberRepository;
    }

    @Transactional(readOnly = true)
    public List<JournalDto> searchJournals(Integer page, String query) {
        List<Journal> journals;
        if (page == null) {
            page = 0;
        }
        Pageable pageable = new PageRequest(page, 10, new Sort(Sort.Direction.DESC, "datePublished"));
        if (isBlank(query)) {
            journals = journalRepository.findAll(pageable).getContent();
        }
        else {
            journals = journalRepository.searchJournals("%" + query + "%", pageable).getContent();
        }
        return journals.parallelStream()
                .map(journal -> {
                    JournalDto.JournalDtoBuilder builder = JournalDto.builder()
                            .journalId(journal.getJournalId())
                            .title(journal.getTitle())
                            .subtitle(journal.getSubtitle())
                            .datePublished(journal.getDatePublished())
                            .uploadedFile(journal.getUploadedFile());
                    List<Publisher> publishers = publisherRepository.findByJournal(journal);
                    List<Subscriber> subscribers = subscriberRepository.findByJournal(journal);
                    publishers.parallelStream()
                            .map(Publisher::getUser)
                            .map(User::getUserId)
                            .forEach(builder::publisherId);
                    subscribers.parallelStream()
                            .map(Subscriber::getUser)
                            .map(User::getUserId)
                            .forEach(builder::subscriberId);
                    return builder.build();
                })
                .collect(toList());
    }

    @Transactional(readOnly = true)
    public JournalDto getJournal(long journalId) {
        Journal journal = journalRepository.findOne(journalId);
        List<Publisher> publishers = publisherRepository.findByJournal(journal);
        List<Subscriber> subscribers = subscriberRepository.findByJournal(journal);
        JournalDto.JournalDtoBuilder builder = JournalDto.builder()
                .journalId(journal.getJournalId())
                .datePublished(journal.getDatePublished())
                .title(journal.getTitle())
                .subtitle(journal.getSubtitle())
                .uploadedFile(journal.getUploadedFile());
        publishers.parallelStream()
                .map(Publisher::getUser)
                .map(User::getUserId)
                .forEach(builder::publisherId);
        subscribers.parallelStream()
                .map(Subscriber::getUser)
                .map(User::getUserId)
                .forEach(builder::subscriberId);
        return builder.build();
    }

    @Transactional
    public void saveJournal(JournalDto journalDto, UploadedFile uploadedFile, Long authorUserId, Set<Long> publisherIds) {
        Date currentDate = new Date();
        User author = userRepository.findOne(authorUserId);
        uploadedFile.setUploadedBy(author);
        uploadedFile.setDateUploaded(currentDate);
        uploadedFileRepository.save(uploadedFile);
        Journal journal = Journal.builder()
                .datePublished(currentDate)
                .title(journalDto.getTitle())
                .subtitle(journalDto.getSubtitle())
                .uploadedFile(uploadedFile)
                .build();
        journalRepository.saveAndFlush(journal);
        publisherIds.stream()
                .map(userRepository::findOne)
                .map(user -> Publisher.builder()
                        .journal(journal)
                        .name(user.getFullname())
                        .user(user)
                        .build())
                .forEach(publisherRepository::save);
        journalDto.setJournalId(journal.getJournalId());
    }

    @Transactional(readOnly = true)
    public List<User> getAllPublishers() {
        return userRepository.findByRole(Roles.PUBLISHER);
    }

    @Transactional
    public Subscriber subscribe(long userId, long journalId) {
        User user = userRepository.findOne(userId);
        Journal journal = journalRepository.findOne(journalId);
        Subscriber subscriber = Subscriber.builder()
                .journal(journal)
                .user(user)
                .dateSubscribed(new Date())
                .build();
        subscriberRepository.save(subscriber);
        return subscriber;
    }

    @Transactional(readOnly = true)
    public List<Subscriber> getSubscriptions(long userId) {
        User user = userRepository.findOne(userId);
        return subscriberRepository.findByUser(user);
    }

    @Transactional
    public void unsubscribe(long subscriberId) {
        subscriberRepository.delete(subscriberId);
    }
}
