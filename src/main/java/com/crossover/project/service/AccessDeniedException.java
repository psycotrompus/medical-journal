package com.crossover.project.service;

/**
 * Created by ej on 7/18/16.
 */
public class AccessDeniedException extends RuntimeException {

    private static final long serialVersionUID = -3356804209559145457L;

    public AccessDeniedException() {
        this("Access denied.");
    }

    public AccessDeniedException(String message) {
        super(message);
    }
}
