package com.crossover.project.service;

import com.crossover.project.entity.UploadedFile;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Singular;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * Created by ejlayco on 7/20/16.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JournalDto implements Serializable {

    private static final long serialVersionUID = 8670667746868832065L;

    private long journalId;

    private String title;

    private String subtitle;

    private Date datePublished;

    private UploadedFile uploadedFile;

    @Singular
    private Set<Long> publisherIds;

    @Singular
    private Set<Long> subscriberIds;
}
