package com.crossover.project.service;

import java.io.Serializable;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class LoginRequestDto implements Serializable {
	
	private static final long serialVersionUID = 137582186461719651L;

	private String username;
	
	private String password;

	private String sessionId;
}
