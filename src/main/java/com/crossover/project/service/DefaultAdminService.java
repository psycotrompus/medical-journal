package com.crossover.project.service;

import com.crossover.project.entity.Roles;
import com.crossover.project.entity.User;
import com.crossover.project.repository.UserRepository;
import org.jasypt.util.password.PasswordEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.StringUtils.isBlank;

@Service
public class DefaultAdminService implements AdminService {

    private UserRepository userRepository;

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    private PasswordEncryptor passwordEncryptor;

    @Autowired
    public void setPasswordEncryptor(PasswordEncryptor passwordEncryptor) {
        this.passwordEncryptor = passwordEncryptor;
    }

    @Transactional(readOnly = true)
    public List<User> searchUsers(Integer page, String query) {
        List<User> users;
        if (page == null) {
            page = 0;
        }
        Pageable pageable = new PageRequest(page, 10);
        if (isBlank(query)) {
            users = userRepository.findAll(pageable).getContent();
        }
        else {
            users = userRepository.searchUsers(query, pageable).getContent();
        }
		return users.parallelStream()
                .filter(u -> u.getRole() != Roles.ADMIN)
                .collect(toList());
	}

    @Transactional(readOnly = true)
    public User getUser(long userId) {
        return userRepository.findOne(userId);
    }

    @Transactional
	public void saveUser(User user) {
        user.setPassword(passwordEncryptor.encryptPassword(user.getPassword()));
        user.setDateRegistered(new Date());
        userRepository.save(user);
	}

    @Transactional
    public User updateUser(User user) {
        User existing = userRepository.findOne(user.getUserId());
        if (existing == null) {
            user = userRepository.save(user);
        }
        else {
            existing.setLastName(user.getLastName());
            existing.setFirstName(user.getFirstName());
            existing.setMiddleName(user.getMiddleName());
            existing.setPassword(user.getPassword());
            existing.setRole(user.getRole());
            user = userRepository.save(existing);
        }
		return user;
	}

    @Transactional
    public boolean deleteUser(long userId) {
        userRepository.delete(userId);
		return true;
	}
}
