package com.crossover.project.util;

import com.crossover.project.entity.UploadedFile;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created by ej on 7/20/16.
 */
public interface FileUtil {

    UploadedFile createUploadedFile(MultipartFile multipartFile);
}
