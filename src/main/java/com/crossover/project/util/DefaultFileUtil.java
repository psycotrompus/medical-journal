package com.crossover.project.util;

import com.crossover.project.entity.UploadedFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.UUID;

public class DefaultFileUtil implements FileUtil {

    public static final String UPLOADS_DIRECTORY = System.getProperty("user.home") + "/uploads";

    private Path uploadPath;

    public DefaultFileUtil() {
        uploadPath = Paths.get(UPLOADS_DIRECTORY);
        File file = uploadPath.toFile();
        if (!file.exists()) {
            file.mkdir();
        }
    }

    public UploadedFile createUploadedFile(MultipartFile multipartFile) {
        StringBuilder filename = new StringBuilder(UUID.randomUUID().toString());
        String originalFilename = multipartFile.getOriginalFilename();
        filename.append(getExtension(originalFilename));
        Path targetFilePath = Paths.get(uploadPath.toString(), filename.toString());
        File targetFile = targetFilePath.toFile();
        try {
            multipartFile.transferTo(targetFilePath.toFile());
        }
        catch (IOException ex) {
            throw new UploadFileException("Unable to save uploaded file.", ex);
        }
        return UploadedFile.builder()
                .dateUploaded(new Date())
                .originalFilename(originalFilename)
                .storedFilename(filename.toString())
                .build();
    }

    private String getExtension(String filename) {
        String[] tokens = filename.split("\\.");
        String extension = tokens[tokens.length - 1];
        if (!extension.startsWith(".")) {
            extension = "." + extension;
        }
        return extension;
    }
}
