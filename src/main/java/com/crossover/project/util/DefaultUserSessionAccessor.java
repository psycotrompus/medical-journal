package com.crossover.project.util;

import com.crossover.project.service.SessionExpiredException;
import com.crossover.project.service.UserDto;

import javax.servlet.http.HttpSession;
import java.io.Serializable;
import java.util.Optional;

import static java.util.Optional.ofNullable;

/**
 * Created by ej on 7/19/16.
 */
public class DefaultUserSessionAccessor implements UserSessionAccessor, Serializable {

    private static final long serialVersionUID = -8476636277712117148L;

    private HttpSession httpSession;

    public DefaultUserSessionAccessor(HttpSession httpSession) {
        this.httpSession = httpSession;
    }

    public void checkExpired() {
        if (hasExpired()) {
            throw new SessionExpiredException(httpSession.getId());
        }
    }

    public boolean hasExpired() {
        return httpSession == null || httpSession.isNew();
    }

    public Optional<UserDto> getUser() {
        UserDto user = (UserDto) httpSession.getAttribute(UserDto.USER_ATTRIBUTE);
        return ofNullable(user);
    }

    public void setUser(UserDto user) {
        httpSession.setAttribute(UserDto.USER_ATTRIBUTE, user);
    }

    public void clearSession() {
        httpSession.removeAttribute(UserDto.USER_ATTRIBUTE);
    }
}
