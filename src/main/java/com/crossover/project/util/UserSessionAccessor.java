package com.crossover.project.util;

import com.crossover.project.service.UserDto;

import java.util.Optional;

/**
 * Created by ej on 7/18/16.
 */
public interface UserSessionAccessor {

    void checkExpired();

    boolean hasExpired();

    Optional<UserDto> getUser();

    void setUser(UserDto user);

    void clearSession();
}
