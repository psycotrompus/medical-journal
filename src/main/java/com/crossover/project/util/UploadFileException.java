package com.crossover.project.util;

public class UploadFileException extends RuntimeException {

    private static final long serialVersionUID = 7391551043769444072L;

    public UploadFileException() {
        this("Problem during uploading file.");
    }

    public UploadFileException(String message) {
        super(message);
    }

    public UploadFileException(String message, Throwable cause) {
        super(message, cause);
    }
}
